---
layout: page
title: Experimental Data Analysis with iPython Notebooks
permalink: projects/edaipynb/
subtitle: Experimental Dataanalysis with iPython Notebooks
---

edaipynb comes with notebooks that you can download from the [github](https://github.com/JanGieseler/edaipynb) page.
Below you can see them as html:

[1) Notebook_and_Python_1-0-1](./html/(1) Notebook_and_Python_1-0-1.html)

[2) Data_in_and_out](./html/(2) Data_in_and_out.html)

[3) Plotting](./html/(3) Plotting.html)

[4) Fitting and error propagation](./html/(4) Fitting and error propagation)

[5) Working with custom packages](./html/(5) Working with custom packages)




