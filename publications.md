---
layout: page
title: Publications
<!-- image: images/patrick-tomasso-Oaqk7qqNh_c-unsplash.jpg -->
---


For most recent information see <a href="https://scholar.google.com/citations?user=6OKJlNgAAAAJ&hl=en&oi=ao"> Google Scholar</a><br>

<ol>
{% for publication in site.data.publications %}
  <li>
  <i>{{ publication.title }}</i><br>
     {{ publication.authors }}<br>
     {{ publication.journal }} <b>{{ publication.volume }}</b> {{ publication.pages }} ({{ publication.year }})<br>
    <a href="{{ publication.url }}">{{ publication.url }}</a><br>
  </li>
{% endfor %}
</ol>